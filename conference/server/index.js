const express = require('express');
const app = express();
const routes = require('./routes');

app.use('/', routes());//routing middleware, reacts to "/routes/index.js file"

app.listen(3000);

module.export = app;