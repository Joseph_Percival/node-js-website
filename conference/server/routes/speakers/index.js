const express = require('express');

const router = express.Router();

//module parent

module.exports = () => {
    router.get('/', (req, res, next) => {
        return res.send('All Speakers'); //needs to implicated into the server index.js file
    });

    router.get('/name', (req, res, next) => {
        return res.send(`Speaker detail page for ${req.params.name}`); //needs to implicated into the server index.js file
    });

    return router;
};