const express = require('express');

const router = express.Router();

//module parent

module.exports = () => {
    router.get('/', (req, res, next) => {
        return res.send('Feedback'); //needs to implicated into the server index.js file
    });

    router.post('/', (req, res, next) => {
        return res.send('Form Sent'); //needs to implicated into the server index.js file
    });

    return router;
};