const express = require('express');

const router = express.Router();

const speakersRoute = require('./speakers');
const feedbackRoute = require('./feedback');
//module parent

module.exports = () => {
    router.get('/', (req, res, next) => {
        return res.send('Index'); //needs to implicated into the server index.js file
    });

    router.use('/speakers', speakersRoute());
    router.use('/feedback', feedbackRoute());

    return router;
};